function NoteComponent(vNode) {
  let editor = {};

  const changeCodeVal = (str) => {
    state[vNode.attrs.index].code = str;
  };
  const executeCode = () => {
    curentIndex = vNode.attrs.index;
    state[vNode.attrs.index].result = "";
    state[vNode.attrs.index].code = editor.getValue();
    //console.log("current index and code : ",vNode.attrs.index, state[vNode.attrs.index].code)
    runPythonCode(state[vNode.attrs.index].code);
  };

  const initCodeMirror = () => {
    var textarea = document.getElementById(
      "txtcode-" + (vNode.attrs.index + 1)
    );
    console.log("id and element", "txtCode-" + vNode.attrs.index, textarea);
    editor = CodeMirror.fromTextArea(textarea, {
      mode: "python",
      theme: "dracula",
      lineNumbers: true,
    });
    // editor.setSize("80%", "150px");
  };

  return {
    oncreate: function (vNode) {
      initCodeMirror();
    },

    view: function (scope) {
      return m("div.note", [
        m("p.noteIndex", "Note(" + (vNode.attrs.index + 1) + ")"),
        m("textarea.txtCode", {
          value: state[vNode.attrs.index]?.code,
          oninput: (e) => changeCodeVal(e.target.value),
          id: "txtcode-" + (vNode.attrs.index + 1),
        }),
        m(
          "button.butrun",
          {
            onclick: async () =>
              await executeCode(state[vNode.attrs.index].code),
          },
          "Run"
        ),
        m("p.noteIndex", "Output(" + (vNode.attrs.index + 1) + ")"),
        m("div.divConsole", state[vNode.attrs.index].result),
      ]);
    },
  };
}
