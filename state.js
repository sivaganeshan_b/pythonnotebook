//@state will contain all the code blocks and results.
//@state[] :{@code: str, @result:str}
let state = [
  {
    code:`
import json
 
# create a sample json
def returnJson():
    return {"name" : "simpleJsonModule", "Topic" : "Json to String", "Method": 1}
     
# Convert JSON to String
def getStrFromJson(a):
    return json.dumps(a)
     
print("Result string : ",getStrFromJson(returnJson()))
print("type : ", type(getStrFromJson(returnJson())))

#Check json.loads function works as expected 
print("Json : ", json.loads(getStrFromJson(returnJson())))`,
    result: ""
  }, 
  {
    code:
      `def doCalc(a,b,operation):
	if operation == "add":
		return a+b;
	elif operation == "sub":
		return a-b;
	elif operation == "mul":
		return a*b;
	else :
		return a/b;
print("add 18 and 6 : ",doCalc(18,6,"add"))
print("sub 19 from 6 : ",doCalc(18,6,"sub"))
print("mul 18 and 6: ",doCalc(19,6,"mul"))
print("div 18 from 6 : ",doCalc(18,6,"div"))`,
    result: "",
  },
  {
    code:
      "def square(n):" + "\n" + "     return n*n;" + "\n" + "print('square of 5 : ',square(5))",
    result: "",
  }
  // {
  //   code:
  //     "print('x from previous window = ',x)" +
  //     "\n" +
  //     "y='sui'" +
  //     "\n" +
  //     "print('changed y value :',y)",
  //   result: "",
  // },
  //,
  // {
  //     code: "import pandas"+"\n"+"mydataset = {"+"\n"+
  //       "'cars': ['BMW', 'Volvo', 'Ford'],"+"\n"+
  //       "'passings': z"+"\n"
  //     +"}"+"\n"+"myvar = pandas.DataFrame(mydataset)"
  //     +"\n"+"print(myvar)",
  //     result: ""
  // },
];
