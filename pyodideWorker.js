let pyodide = {};
let pythonWorker = {};

let validationCode = `import ast
tree = ast.parse(
"""###code###"""
,mode='exec')
error = False
for node in tree.body:
  if isinstance(node, ast.FunctionDef):
      continue
  elif isinstance(node, ast.Expr):
      continue
  elif isinstance(node, ast.Import):
      continue
  elif isinstance(node, ast.ImportFrom):
      continue
  elif isinstance(node, ast.alias): 
      continue
  else:
      print("only FunctionDef, Expr are allowed !!! Assign Variables inside a Func")
      error = True
      break
`;

const updateResultState = async (str) => {
  if (str.indexOf("complete") === -1 && str.indexOf("warn") === -1) {
    postMessage({ str: str });
  }
};

const initPyodide = async () => {
  importScripts("https://cdn.jsdelivr.net/pyodide/v0.19.1/full/pyodide.js");
  loadPyodide({
    indexURL: "https://cdn.jsdelivr.net/pyodide/v0.19.1/full/",
    stdout: (s) => {
      updateResultState(s.toString() + "\n");
    },
    stderr: (s) => {
      updateResultState(s.toString() + "\n");
    },
  }).then((initialisedObj) => {
    pyodide = initialisedObj;
    //   pyodide.loadPackage("pandas").then(() => {
    //   console.log("pyodide packages Loaded : ", pyodide);
    // });
    postMessage({ type: "loading", isloading: false });
  });
};
initPyodide();

const validateCodeBeforeExecution = async (codeTxt) => {
  let astCheckCode = validationCode.replace("###code###", codeTxt);
  //console.log(astCheckCode);
  pyodide.runPython(astCheckCode);
};

onmessage = async function (e) {
  //console.log("code :", e.data.code);
  await validateCodeBeforeExecution(e.data.code);
  let isErrorInCodeValidation = await pyodide.globals.get("error");
  // console.log("isErrorInCodeValidation : ", isErrorInCodeValidation);
  if (!isErrorInCodeValidation) {
    pyodide.runPython(e.data.code);
  }
};
