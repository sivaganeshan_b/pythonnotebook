var root = document.body;
let pythonWorker = {};
let curentIndex = 0;
let loading = true;

handleOnMessage = function (e) {
  if (e.data.type == "loading") {
    loading = e.data.isloading;
    m.redraw();
  } else {
    state[curentIndex].result += e.data.str;
    m.redraw();
  }
};

if (window.Worker) {
  pythonWorker = new Worker("./pyodideWorker.js");
  pythonWorker.addEventListener("message", handleOnMessage);
  console.log("python worker : ", pythonWorker);
}

const runPythonCode = async (codeTxt) => {
  pythonWorker.postMessage({ code: codeTxt });
};

const createNewBook = () => {
  state.push({ code: "", result: "" });
};

m.mount(root, {
  view: function (scope) {
    return loading ? m("div.loading") : m(appcomponent);
  },
});
