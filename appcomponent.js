function appcomponent(vNode) {
  return {
    view: function () {
      return m("div", [
        m("div.noteTitle", "Python NoteBook"),
        m(NoteBookComponent),
        m(
          "button.butNew",
          {
            onclick: () => createNewBook(),
          },
          "New Note"
        ),
      ]);
    },
  };
}
